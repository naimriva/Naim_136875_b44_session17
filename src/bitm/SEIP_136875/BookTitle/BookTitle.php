<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 11:59 AM
 */

namespace App\BookTitle;
use App\Model\Database as DB;

class BookTitle extends DB
{
    private $id;
    private $book_name;
    private $author_name;

    public function setData($postData)
    {
       if (array_key_exists('id',$postData))
       {
           $this->id=$postData['id'];
       }

        if (array_key_exists('bookName',$postData))
            {
                $this->book_name=$postData['bookName'];
            }
        if (array_key_exists('authorName',$postData))
                {
                    $this->author_name=$postData['authorName'];
                }
    }

    public function store()
    {
        $arrData=array($this->book_name.$this->author_name);

        $sql="insert into book_title(book_name,author_name) VALUES (?,?)";

        //sth=statement handel
        $sth=$this->dbh->prepare(sql);

        $result=$sth->execute($arrData);

        Utility::redirect('create.php');
    }

    public function index()
    {
        echo "i'm inside :". __METHOD__;
    }

}
